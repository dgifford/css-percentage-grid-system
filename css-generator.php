<?php

/*
	Defaults
 */
$prefix = 'w_';
$seperator = '_';
$denominators = array( 2, 3, 4, 5, 6, 8, 9, 10, 12, 16, );
$max_page_width = '1200px';
$gutter_width = '10px';
$double_gutter_width = '20px';
$clear_full_row = 1;



/*
	Overwrite defaults with values from URL
	Only un-comment this in development as there 
	is no sanitisation of the URL parameters
 */
/*
if ( !empty($_GET) )
{
	extract( $_GET );
}
*/

$column_css = '';


/*
	Calculate column widths as percentages
 */
foreach( $denominators as $denominator )
{
	// Add CSS comment
	$column_css .= "\n\n/*  1/{$denominator}'s */\n";

	// Loop to calculate widths
	for( $numerator = 1; $numerator < $denominator ; $numerator++ )
	{
		$percentage_width = number_format( (( $numerator / $denominator ) * 100), 4);
		$column_css .= ".{$prefix}{$numerator}{$seperator}{$denominator}{width: {$percentage_width}%;}\n";
	}

	// Add clear full row class if required
	if( $clear_full_row == 1 )
	{
		$denominator_plus_one = $denominator + 1;
		$column_css .= ".clearfull .{$prefix}1{$seperator}{$denominator}:nth-child({$denominator}n+{$denominator_plus_one}) {clear:left;}\n";
	}
}



/*
	tell browser this is a css document
 */
header('Content-type: text/css');

/*
	Output
 */
print <<<CSS

/*
	DG Percentage Grid System
************************************************/



/*
	Border box reset, dee http://www.paulirish.com/2012/box-sizing-border-box-ftw/
	Includes prefixes for older browsers
*/
html
{
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
}

*, *:before, *:after
{
-webkit-box-sizing: inherit;
-moz-box-sizing: inherit;
box-sizing: inherit;
}



/*
	Main containter
	Defines maximum page width
 */
.container
{
max-width:{$max_page_width}; 
margin:0 auto;
}



/*
	Row container
 */ 
.row
{
width:100%;
clear:both;
}

/* row clearfix */ 
.row:before 
,.row:after
{
content:"";
display: table ;
clear:both;
}



/* 
	Structural column widths 
	Defined as fractions 
*/

{$column_css}



/*
	Column width defaults 
	Match selectors starting with {$prefix} and containing {$prefix}
*/ 
[class^='{$prefix}'], [class*=' {$prefix}']
{
float: left;
min-height: 1px; /* stop empty columns stacking */
}



/* 
	Spacing and gutter classes 
*/
.p
{
padding: {$gutter_width};
}

.m
{
margin: {$gutter_width};
}

.pp
{
padding: {$double_gutter_width};
}

.mm
{
margin: {$double_gutter_width};
}


CSS;


exit;
?>
