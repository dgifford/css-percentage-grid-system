# CSS Percentage Grid System #


A simple, light-weight float and padding based CSS grid system using fractional percentage widths.


Columns widths are defined by applying a 'fractional' class, e.g:

* halfs ```class="w_1_2"```
* eigths ```class="w_1_8"```
* tenths ```class="w_1_10"```
* three tenths ```class="w_3_10"```


Container, row and gutter classes are also included


Here is a code fragment, laying out the elements of a header:

```
<header>
	<div class="container">
		<div class="row">

			<div class="w_1_2 p logo">
				<a href="http://www.example.com"><img src="http://www.example.com/logo.png"></a>
			</div>


			<div class="w_1_2">

				<div class="w_1_2 p telephone">
					<a href="tel:+1111111111">+44 1111 111111</a>
				</div>

				<div class="w_1_2 p email">
					<a href="mailto:hello@example.com">hello@example.com</a>
				</div>			

			</div>

		</div>
	</div>
</header>
```


A PHP script is included, 'css-generator.php' for calculating the percentage widths and generating the CSS.

## css-generator.php ##

css-generator.php calculates the percentage widths for the columns (better than relying on my maths) and allows other parts of CSS to be customised. It outputs the resulting CSS.

**Note:** css-generator.php should not be used in production as no user input is sanitised.

The defaults can be modified using URL parameters (uncomment lines 22-25 for this to work):

* 'prefix' - The string that goes before the percentage column, defaults to 'w_'
* 'seperator' - The string between the numerator and denominator of the fraction, defaults to  '_'
* 'denominators' - Denominators used, defaults to  array( 2, 3, 4, 5, 6, 8, 9, 10, 12, 16, )
* 'max_page_width' - The maximum width of the container, defaults to '1200px'
* 'gutter_width' - The gutter width, defaults to  '10px'
* 'double_gutter_width' - Double the gutter width, defaults to  '20px'
* 'clear_full_row' - Whether to clear the first column in a line, e.g. the 4th div in this example:
```
<div class="row clearfull">		<!-- 'clearfull' class applied to containing row -->
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
	<div class="w_1_3"></div> 	<!-- This div will have clear left applied to it -->
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
	<div class="w_1_3"></div> 	<!-- This div will have clear left applied to it -->
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
</div>
```

Of course, the above could be achieved by having more containers with the 'row' class:
```
<div class="row">		
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
</div>
<div class="row">		
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
</div>
<div class="row">		
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
	<div class="w_1_3"></div>
</div>

```


Using 'clearfull' might be easier when the columns are dynamically generated or to improve the layout on smaller screen sizes.


css-generator.php example:

* css-generator.php?denominators[]=2&denominators[]=3&denominators[]=4&gutter_width=20px&max_page_width=960px